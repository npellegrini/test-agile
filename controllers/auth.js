﻿const express = require('express');
const router = express.Router();
const authService = require('../services/auth');
const middlewareValidationAuth = require('../middleware/auth');
const { check, param, validationResult, body } = require('express-validator');
var errorFormatter = require('../_helpers/error-formatter');

// routes
router.post('/', middlewareValidationAuth.auth, auth);

//Const
const resObject = {
  valid: false,
  message: 'Upps disculpe no se cumple con las especificaciones requeridas.',
  errors: null,
  data: null
}

module.exports = router;

function auth(req, res, next) {
  let body = req.body;
  // Finds the validation errors in this request and wraps them in an object with handy functions and formating the msg errors
  const result = validationResult(req).formatWith(errorFormatter);
  if (!result.isEmpty()) {
    resObject.errors = result.array();
    return res.status(200).json(resObject);
  }
  
  authService
    .auth(body)
    .then(response =>
      res.status(200).json(response)
    )
    .catch(err => {
      resObject.valid = false;
      resObject.message = "An error has ocurred.";
      resObject.errors = err;
      resObject.data = null;
      return res.status(200).json(resObject);
    });
}

