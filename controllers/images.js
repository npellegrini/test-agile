const express = require('express');
const router = express.Router();
const imagesService = require('../services/images');
const middlewareValidationImages = require('../middleware/images');
const { check, param, validationResult, body } = require('express-validator');
var errorFormatter = require('../_helpers/error-formatter');

// routes
router.get('/', middlewareValidationImages.cache(10),getAll);
router.get('/:idPicture', [middlewareValidationImages.getById, middlewareValidationImages.cache(10)], getById);



//Const
const resObject = {
    valid: false,
    message: 'Upps disculpe no se cumple con las especificaciones requeridas.',
    errors: null,
    data: null
}

module.exports = router;


function getAll(req, res, next) {
    let query = req.query;
    //Split token
    let token = req.get('Authorization').split(" ")[1];

    imagesService
        .getAll( query, token)
        .then(response => {


            res.status(response.statusCode).json(response);
        })
        .catch(err => {

            return res.status(500).json(err);
        });
}

function getById(req, res, next) {
    // Finds the validation errors in this request and wraps them in an object with handy functions and formating the msg errors
    const result = validationResult(req).formatWith(errorFormatter);
    if (!result.isEmpty()) {
        resObject.valid = false;
        resObject.data = null;
        resObject.errors = result.array();
        return res.status(200).json(resObject);
    }
    //Split token
    let token = req.get('Authorization').split(" ")[1];

    imagesService
        .getById(req.params, token)
        .then( response => {
            return res.status(response.statusCode).json(response);
        })
        .catch(err => {
            return res.status(500).json(err);
        });
}















