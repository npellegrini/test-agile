const express = require('express');
const router = express.Router();
const searchService = require('../services/search');
const middlewareValidationSearch = require('../middleware/search');
const { check, param, validationResult, body } = require('express-validator');
var errorFormatter = require('../_helpers/error-formatter');

// routes
router.get('/:searchTerm', [middlewareValidationSearch.search, middlewareValidationSearch.cache(10)],search);



//Const
const resObject = {
    valid: false,
    message: 'Upps disculpe no se cumple con las especificaciones requeridas.',
    errors: null,
    data: null
}

module.exports = router;




function search(req, res, next) {
    // Finds the validation errors in this request and wraps them in an object with handy functions and formating the msg errors
    const result = validationResult(req).formatWith(errorFormatter);
    if (!result.isEmpty()) {
        resObject.valid = false;
        resObject.data = null;
        resObject.errors = result.array();
        return res.status(200).json(resObject);
    }
    //Split token
    let token = req.get('Authorization').split(" ")[1];

    searchService
        .search(req.params, token)
        .then( response => {
            return res.status(response.statusCode).json(response);
        })
        .catch(err => {
            return res.status(err.statusCode).json(err);
        });
}















