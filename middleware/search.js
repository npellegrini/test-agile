const { check, param, validationResult, body } = require('express-validator');
var mcache = require('memory-cache');


//Here we could put all the validation that we need to validate de id that was inserted
var search = [
    check('searchTerm')
        .trim().escape().exists({ checkFalsy: true }).withMessage("Can't be empty")
];


var cache = (duration) => {
    return (req, res, next) => {
        let key = '__express__' + req.originalUrl || req.url;
        let cachedBody = mcache.get(key);
        if (cachedBody) {
            cachedBody = JSON.parse(cachedBody);
            
            res.status(cachedBody.statusCode).json(cachedBody);
            return
        } else {
            res.sendResponse = res.send;
            res.send = (body) => {
                mcache.put(key, body, duration * 1000);
                res.sendResponse(body);
            }
            next();
        }
    }
}

module.exports = {
    search,
    cache
};