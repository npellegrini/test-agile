﻿const { check } = require('express-validator');

var auth = [
  check('apiKey')
    .trim().escape().exists({ checkFalsy: true }).withMessage("No puede estar vacio."),
];

module.exports = {
  auth
};