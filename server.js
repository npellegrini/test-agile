﻿//Requires
require('rootpath')();
const express = require('express');
const app = express();
//Enviroment
const dotenv = require("dotenv");
dotenv.config({
    path: ".env"
});

const initService = require('./services/init');


const bodyParser = require('body-parser');

const jwtMiddleware = require('_helpers/jwt');
const errorHandler = require('_helpers/error-handler');

// CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credentials", "true");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    next();
});

app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));

// app.use(cors(corsOptions));
app.disable('x-powered-by');

// use JWT auth to secure the api
app.use(jwtMiddleware())

// global error handler
app.use(errorHandler);

//Import Routes
var imagesRoute = require('./controllers/images');
var authRoute = require('./controllers/auth');
var searchRoute = require('./controllers/search');

// api routes
app.use('/auth', authRoute);
app.use('/images', imagesRoute);
app.use('/search', searchRoute);

init()
// start server
const port = process.env.PORT || CONFIG.PORT;
const server = app.listen(port, function () {
    console.log('Server puerto ' + port + ': \x1b[32m%s\x1b[0m', 'online');
});



async function init(){
    initService.init()
}