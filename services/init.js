﻿
const axios = require('axios').default;
const fs = require('fs');


const resObject = {};

module.exports = {
  init,
  init2
};
/***********************
* User user 
***********************/
async function init() {
  try {
    const bodyAxious = JSON.stringify({ apiKey: process.env.API_KEY });
    let config = {
      method: 'post',
      url: process.env.API_URL + '/auth',
      headers: {
        'Content-Type': 'application/json',
      },
      data: bodyAxious

    };

    let responseAuth = await axios(config);

    
    let url = process.env.API_URL + '/images';

    config={};
    config = {
      method: 'get',
      url,
      headers: {
        'Authorization': 'Bearer ' + responseAuth.data.token,
        'Content-Type': 'application/json',
      }
    };
    
    let responseImages = await axios(config);
    let pictures = [];

    if (responseImages.data.pictures.length > 0) {
      pictures = [...responseImages.data.pictures]
    }

    if (responseImages.data.pageCount > 1) {
      for (let index = 2; index <= responseImages.data.pageCount; index++) {
        let urlPage = process.env.API_URL + '/images?page='+index;
        config.url = urlPage;
        responseImages = await axios(config);
        pictures = [...pictures,...responseImages.data.pictures];
      }
    }
    let newArrayPictures = [];
    for (let picture of pictures) {
      config.url = process.env.API_URL + '/images/' + picture.id;
      let responseImagesById = await axios(config);
      newArrayPictures.push({...picture, ...responseImagesById.data});
    }

    fs.writeFileSync(`./uploads/info.json`, JSON.stringify(newArrayPictures));
    return responseImages.data;
  } catch (error) {
    resObject.message = error.message;
    resObject.auth = false;
    return resObject;
  }

}
async function init2() {
  try {
    let contentTxt = fs.readFileSync(`./uploads/info.json`, 'utf8');
  } catch (error) {
    return resObject;
  }

}
