var fs = require('fs');

const resObject = {};

module.exports = {
  search
};




async function search(params, token) {

  try {
    var regex = new RegExp(params.searchTerm, 'i');
    let infoJson = JSON.parse(fs.readFileSync(`./uploads/info.json`, 'utf8'));

    if (infoJson.length > 0) {
      var result = infoJson.filter((picture) => {
        return regex.test(picture.camera) || regex.test(picture.author)
       })
    }
    if (result.length > 0) {
      return {statusCode: 201,valid: true, result};
    }
    return {statusCode: 404,valid: false, message: "It hasn't found it"};
  } catch (error) {
    resObject.message = error.message;
    resObject.statusCode = 500;
    resObject.valid = false;
    return resObject;
  }
}

