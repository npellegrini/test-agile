﻿const axios = require('axios').default;

const resObject = {};

module.exports = {
  auth
};
/***********************
* User user 
***********************/
async function auth({ apiKey }) {

  if (apiKey) {

    try {
      const bodyAxious = JSON.stringify({ apiKey });
      let config = {
        method: 'post',
        url: process.env.API_URL + '/auth',
        headers: {
          'Content-Type': 'application/json',
        },
        data: bodyAxious

      };

      let response = await axios(config);


      return response.data;
    } catch (error) {
      resObject.message = error.message;
      resObject.auth = false;
      return resObject;
    }
  }
  resObject.message = 'Incorrect credentials';
  resObject.data = null;
  resObject.valid = false;
  return resObject;
}
