const axios = require('axios').default;

const resObject = {};

module.exports = {
  getAll,
  getById
};



async function getAll(query, token) {

  let url = process.env.API_URL + '/images';

  try {
    if (query.hasOwnProperty("page")) {
      url+="?page="+query.page;
    }

    let config = {
      method: 'get',
      url,
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      }
    };

    let response = await axios(config);


    return {valid: true,statusCode: response.status, ...response.data};
  } catch (error) {
    resObject.statusCode = error.response.status;
    resObject.message = error.message;
    resObject.valid = false;
    return resObject;
  }
}

async function getById(params, token) {

  
  try {
    let url = process.env.API_URL + '/images/'+params.idPicture;

    let config = {
      method: 'get',
      url,
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json',
      }
    };

    let response = await axios(config);

    return {valid: true,statusCode: response.status, ...response.data};
  } catch (error) {
    resObject.statusCode = error.response.status;
    resObject.message = error.message;
    resObject.status = error.response.data.status;
    resObject.valid = false;
    return resObject;
  }
}

