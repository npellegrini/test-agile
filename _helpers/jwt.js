const expressJwt = require('express-jwt');

module.exports = jwtMiddleware;

function jwtMiddleware() {
  const secret = process.env.JWT_KEY;
  return expressJwt({ secret }).unless({
    path: [
      // public routes that don't require authentication
      '/',
      '/auth',
      '/images',
      /^\/images\/.*/,
      /^\/search\/.*/,
    ]
  });
}


